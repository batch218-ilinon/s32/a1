let http = require("http");

http.createServer(function (request, response) {
	if(request.url == "/archiveCourse" && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		// Ends the response process
		response.end('Archive courses to our resources');
	}
	if(request.url == "/updateCourse" && request.method == "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		// Ends the response process
		response.end('Update a course to our resources');
	}
	if(request.url == "/addCourse" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		// Ends the response process
		response.end('Add course to our resources');
	}
	if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		// Ends the response process
		response.end("Here's our courses available");
	}
	if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		// Ends the response process
		response.end('Welcome to your profile');
	}
	if(request.url == "/" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		// Ends the response process
		response.end('Welcome to booking system');
	}
}).listen(4000);

console.log(`Server is running at localhost: 4000`)